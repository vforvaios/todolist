import "../styles/main.scss"

/* VARIABLES --------------------------------- */
let todolist = document.querySelector("#todoitems")
let tododescription = document.querySelector("#tododescription")
let _addTodoBtn = document.querySelector("#addtodo")
let lielement, element2
let todoItems = []
let todoItem
let counter = 0

/* FUNCTIONS --------------------------------- */
function printTodoItems() {
  for(let x=0; x<todoItems.length; x++) {
    todoItems[x].printMe()
  }
}

function addTodoItem() {
  todolist.textContent = ''
  counter = (todoItems.length == 0) ? 0 : parseInt(Math.max.apply(todoItems, todoItems.map(obj => obj.uniqueid)) + 1)

  todoItem = new TodoItem(tododescription.value, counter)
  todoItem.addItem()

  printTodoItems()
}

function removeTodoItem(dataid) {
  todolist.textContent = ''

  todoItems = todoItems.filter(obj => obj.uniqueid != dataid)

  printTodoItems()
}

/* CLASSES ----------------------------------- */
class TodoItem {
  constructor(val, uniqueid) {
    this.val = val
    this.uniqueid = uniqueid
  }

  addItem() {
    todoItems.push(this)
    tododescription.value = ''
    tododescription.focus()
  }

  printMe() {
    lielement = document.createElement("li")
    lielement.setAttribute('data-id', this.uniqueid)
    element2 = document.createElement("span")
    var elementtext = document.createTextNode(this.val)
    element2.appendChild(elementtext)
    lielement.appendChild(element2)
    todolist.appendChild(lielement)

    lielement.addEventListener('click', function() {
      removeTodoItem(this.getAttribute('data-id'))
    })
  }
}

/* EVENT LISTENERS -------------------------- */
_addTodoBtn.addEventListener("click", addTodoItem)
